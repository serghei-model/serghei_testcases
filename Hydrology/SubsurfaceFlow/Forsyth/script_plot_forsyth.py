"""
    Verify serghei subsurface solver with Forsyth's free-drainage problem

    Experimental results :

        wc = {
            1 day   :   [0.279, 0.3, 0.31, 0.318, 0.32]
                        [-0.4, -1.15, -1.92, -2.71, -3.42]
            4 day   :   [0.24, 0.255, 0.261, 0.278]
                        [-0.4, -1.15, -1.92, -2.71]
            20 day  :   [0.198, 0.209, 0.214, 0.23, 0.24]
                        [-0.4, -1.15, -1.92, -2.71, -3.42]
            100 day :   [0.163, 0.176, 0.175, 0.189, 0.20]
                        [-0.4, -1.15, -1.92, -2.71, -3.42]
        }

"""

import numpy as np
import matplotlib.pyplot as plt

fdir = 'out/'
ind = ['2','5','21','101']
flabel = ['1 day', '4 days','20 days','100 days']
N = 100
zVec = np.linspace(-6.0, 0.0, N)
color = ['b','r','g','m']

z_expr = [-0.4, -1.15, -1.92, -2.71, -3.42]
wc_expr = [[0.279, 0.3, 0.31, 0.318, 0.32],
    [0.24, 0.255, 0.261, 0.278, np.nan],
    [0.198, 0.209, 0.214, 0.23, 0.24],
    [0.163, 0.176, 0.175, 0.189, 0.20]]

h_out = []
wc_out = []
for ii in range(len(ind)):
    fname = fdir + 'result_subsurface'+str(ind[ii])+'.vtk'
    fid = open(fname, 'r')
    while True:
        line = fid.readline()
        if line[:9] == 'SCALARS h':
            h = []
            line = fid.readline()
            for ii in range(N):
                line = fid.readline()
                h.append(float(line))
            h_out.append(h)
        elif line[:10] == 'SCALARS wc':
            wc = []
            line = fid.readline()
            for ii in range(N):
                line = fid.readline()
                wc.append(float(line))
            wc_out.append(wc)
            break

h_out = np.flipud(np.array(h_out))
wc_out = np.fliplr(np.array(wc_out))

plt.figure(1, figsize=[4.5, 7])
for ii in range(len(ind)):
    plt.plot(wc_expr[ii], z_expr, linestyle='None',
        marker='x', markerfacecolor='None', color=color[ii])
    plt.plot(wc_out[ii,:], zVec, color=color[ii], label=flabel[ii])
plt.xlabel('Water Content')
plt.ylabel('Z [m]')
plt.legend(loc='lower left')
plt.show()
