"""
    Create top boundary condition for Tracy's problem
"""
import numpy as np
import matplotlib.pyplot as plt
import math
from analytical import *

nx = 40
ny = 24
dx = 0.25
dy = 0.25

gamma = 0.1634
h0 = -12.0
L = nx*dx
W = ny*dy
H = 8.0

head = np.zeros((nx,ny), dtype=float)

solver = Tracy_analytic([L,W,H], 1e-4, 0.35, 0.016, gamma)
for ii in range(nx):
    x = (ii+0.5)*dx
    for jj in range(ny):
        y = (jj+0.5)*dy
        head[ii,jj] = solver.get_solution(x, y, H, 0.0, h0)


fid = open('hini.input','w')
fid.write('NCOLS 40\n')
fid.write('NROWS 24\n')
fid.write('XLLCORNER 0\n')
fid.write('YLLCORNER 0\n')
fid.write('CELLSIZE 0.25\n')
fid.write('NODATA_value -9999\n')
for jj in range(ny):
    row = str(round(head[0,jj],6))
    for ii in range(1, nx):
        row += ' '
        row += str(round(head[ii,jj],6))
    fid.write(row+'\n')
fid.close()



plt.figure(1)
plt.imshow(head, cmap='jet')


plt.show()
