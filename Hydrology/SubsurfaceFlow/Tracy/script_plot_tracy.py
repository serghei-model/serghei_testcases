"""
    Verify serghei subsurface solver with Tracy's analytical solution

"""
import numpy as np
import matplotlib.pyplot as plt
from analytical import *

"""
    --------------------------------------------------------------------
                        Load SERGHEI outputs
    --------------------------------------------------------------------
"""

fdir = 'out/'
ind = ['2']
dim = [40, 24, 32]
N = int(dim[0]*dim[1]*dim[2])
zVec = np.linspace(-6.0, 0.0, N)
x_bound = 20
y_slice = 11

h_out = []
wc_out = []
for ii in range(len(ind)):
    fname = fdir + 'result_subsurface'+str(ind[ii])+'.vtk'
    fid = open(fname, 'r')
    while True:
        line = fid.readline()
        if line[:9] == 'SCALARS h':
            h = []
            line = fid.readline()
            for jj in range(N):
                line = fid.readline()
                h.append(float(line))
            h_out.append(h)
        elif line[:10] == 'SCALARS wc':
            wc = []
            line = fid.readline()
            for jj in range(N):
                line = fid.readline()
                wc.append(float(line))
            wc_out.append(wc)
            break

h_out = np.reshape(np.array(h_out), (dim[0],dim[1],dim[2]), order='F')
wc_out = np.reshape(np.array(wc_out), (dim[0],dim[1],dim[2]), order='F')
print('Dimension of SERGHEI output = ',np.shape(wc_out))

"""
    --------------------------------------------------------------------
                            Analytical Solution
    --------------------------------------------------------------------
"""
L = 10.0
W = 6.0
H = 8.0
#   soil properties
wcs = 0.35
wcr = 0.016
Ks = 1e-4
ga = 0.1634
#   grid resolutions
dx = [0.25, 0.25, 0.25]
#   initial / boundary condition
h0 = -12.0
#   Time step
tvec = np.linspace(0, 9038, 2)
#   build domain
out = np.zeros((int(L/dx[0]), int(W/dx[1]), int(H/dx[2]), len(tvec)))
print('Dimension of analytical solution = ',np.shape(out))
'''
    Get the analytical solution
'''
solver = Tracy_analytic([L,W,H], Ks, wcs, wcr, ga)
for ii in range(np.shape(out)[0]):
    x = (ii+0.5)*dx[0]
    for jj in range(np.shape(out)[1]):
        y = (jj+0.5)*dx[1]
        for kk in range(np.shape(out)[2]):
            z = (kk+0.5)*dx[2]
            for tt in range(len(tvec)):
                out[ii,jj,kk,tt] = solver.get_solution(x, y, z, tvec[tt], h0)

"""
    --------------------------------------------------------------------
                                Make plot
    --------------------------------------------------------------------
"""
relerr3d = (np.transpose(h_out[:,y_slice,:]) - np.flipud(np.transpose(out[:,y_slice,:,-1])))/ np.flipud(np.transpose(out[:,y_slice,:,-1]))
maxerr = np.amax(relerr3d)
avgerr = np.mean(relerr3d)
print(' Max err = ',maxerr)
print(' Avg err = ',avgerr)

relerr = (np.transpose(h_out[:,y_slice,:]) - np.flipud(np.transpose(out[:,y_slice,:,-1]))) / np.flipud(np.transpose(out[:,y_slice,:,-1]))

plt.figure(1, figsize=[12, 5])

plt.subplot(1,3,1)
plt.imshow(np.transpose(h_out[:x_bound,y_slice,:]), vmin=-12, vmax=0, cmap='jet')
plt.colorbar()
xlabel = [0, 2.5, 5]
xtick = np.linspace(0,int(dim[0]/2-1),3)
ylabel = [0, -4, -8]
ytick = np.linspace(0,dim[2]-1,3)
plt.xticks(xtick,xlabel)
plt.yticks(ytick,ylabel)
plt.xlabel('X [m]')
plt.ylabel('Z [m]')
plt.title('Serghei')

plt.subplot(1,3,2)
plt.imshow(np.flipud(np.transpose(out[:x_bound,y_slice,:,-1])), vmin=-12, vmax=0, cmap='jet')
plt.colorbar()
xlabel = [0, 2.5, 5]
xtick = np.linspace(0,int(dim[0]/2-1),3)
ylabel = [0, -4, -8]
ytick = np.linspace(0,dim[2]-1,3)
plt.xticks(xtick,xlabel)
plt.yticks(ytick,ylabel)
plt.xlabel('X [m]')
plt.ylabel('Z [m]')
plt.title('Analytical')

plt.subplot(1,3,3)
plt.imshow(abs(relerr[:,:x_bound]), vmin=0.0, vmax=0.05, cmap='jet')
plt.colorbar()
xlabel = [0, 2.5, 5]
xtick = np.linspace(0,int(dim[0]/2-1),3)
ylabel = [0, -4, -8]
ytick = np.linspace(0,dim[2]-1,3)
plt.xticks(xtick,xlabel)
plt.yticks(ytick,ylabel)
plt.xlabel('X [m]')
plt.ylabel('Z [m]')
plt.title('Relative Error')

#plt.savefig('fig_tracy.eps',format='eps')

plt.show()
