"""
    Verify serghei subsurface solver with Warrick's solution

    Warrick's solution :
        time = [11700, 23400, 46800]
        wc   = [0.0825, 0.165, 0.2475]
        z1   = [74.58, 75.20, 77.12]
        z2   = [61.32, 62.13, 64.54]
        z3   = [39.02, 40.04, 42.80]

"""

import numpy as np
import matplotlib.pyplot as plt

fdir = ['out-pca/','out-gmres/']
lgd = ['PCA', 'Hybrid']

ind = ['1','2','4']
N = [200, 200, 200]
analytical = np.array([[0.0825, 0.165, 0.2475], [74.58, 75.20, 77.12],
    [61.32, 62.13, 64.54], [39.02, 40.04, 42.80]])
color = ['b','r','g']
lstyle = [':', '--', '-']

fs = 12

h_out = []
wc_out = []

for ii in range(len(ind)):
    for ff in range(len(fdir)):
        fname = fdir[ff] + 'result_subsurface'+str(ind[ii])+'.vtk'
        fid = open(fname, 'r')
        while True:
            line = fid.readline()
            if line[:9] == 'SCALARS h':
                h = []
                line = fid.readline()
                for jj in range(N[ff]):
                    line = fid.readline()
                    h.append(float(line))
                h_out.append(h)
            elif line[:10] == 'SCALARS wc':
                wc = []
                line = fid.readline()
                for jj in range(N[ff]):
                    line = fid.readline()
                    wc.append(float(line))
                wc_out.append(wc)
                break

# h_out = np.flipud(np.array(h_out))
# wc_out = np.fliplr(np.array(wc_out))

plt.figure(1, figsize=[4, 6])
idat = 0

for ii in range(len(ind)):
    for ff in range(len(fdir)):
        zVec = np.linspace(-1.0, 0.0, N[ff])
        plt.plot(np.flipud(np.array(wc_out[idat])), zVec, color=color[ii], ls=lstyle[ff])
        idat += 1
    plt.plot(analytical[0,:], -(1.0-analytical[ii+1,:]/100.0), linestyle='None',
        marker='x', markerfacecolor='None', color=color[ii])

plt.xlabel('Water Content')
plt.ylabel('Z [m]')
plt.legend(lgd, fontsize=fs)

#plt.savefig('fig_warrick.eps',format='eps')
plt.show()
