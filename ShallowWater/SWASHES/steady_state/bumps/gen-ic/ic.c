#include <stdio.h>
#include <math.h>

#define DEBUG 1

#define N 1000
#define R 4
#define L 25.0
#define E 0.5

int main(void)
{

  double x[N];
  double z[N];
  double h[N];
  double dx = L / N;

  for (int i = 0; i < N; i ++)
    {
      x[i] = dx * i;
      z[i] = (x[i] > 8.0 && x[i] < 12.0) ? 0.2 - 0.05 * (x[i] - 10.0) * (x[i] - 10.0) : 0.0;
      h[i] = E - z[i];
      if (h[i] < 0.0)
	h[i] = 0.0;
      #ifdef DEBUG
      printf("%f %f %f\n", x[i], z[i], h[i]);
      #endif
    }

  FILE *fp = fopen("dem.input", "w");
  FILE *hp = fopen("hini.input", "w");
  
  fprintf(fp, "NCOLS %d\n", N);
  fprintf(fp, "NROWS %d\n", R);
  fprintf(fp, "XLLCORNER 0\n");
  fprintf(fp, "YLLCORNER 0\n");
  fprintf(fp, "CELLSIZE %f\n", dx);
  fprintf(fp, "NODATA_value -9999\n");

  fprintf(hp, "NCOLS %d\n", N);
  fprintf(hp, "NROWS %d\n", R);
  fprintf(hp, "XLLCORNER 0\n");
  fprintf(hp, "YLLCORNER 0\n");
  fprintf(hp, "CELLSIZE %f\n", dx);
  fprintf(hp, "NODATA_value -9999\n");

  for (int r = 0; r < R; r ++)
    {
      for (int i = 0; i < N; i ++)
	{
	  fprintf(fp, "%f ", z[i]);
	  fprintf(hp, "%f ", h[i]);
	}
      fprintf(fp, "\n");
      fprintf(hp, "\n");
    }

  fclose(hp);
  fclose(fp);
  
  return 0;
  
}
