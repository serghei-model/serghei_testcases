#include <stdio.h>
#include <math.h>

#define G 9.81
#define X 1000.0

int main() {

  double hex = pow(4.0/G, 1.0/3.0) *
    (1.0 + 0.5 * exp(-16.0 * (X / 1000.0 - 0.5) * (X / 1000.0 - 0.5)));

  printf("  hex(%f) = %f\n", X, hex);

  return 0;
  
}
